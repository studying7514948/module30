import pytest
# from flask import template_rendered
from app import create_app, db as _db
from app.models import Client, Parking, ClientParking


@pytest.fixture
def app():
    _app = create_app()
    _app.config["TESTING"] = True
    _app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite://"

    with _app.app_context():
        _db.create_all()
        client = Client(name='Семен',
                        surname='Семенов',
                        credit_card='123456789',
                        car_number='x777xx34')
        parking = Parking(address='Советская 1а',
                          opened=True,
                          count_places=50,
                          count_available_places=2)
        _db.session.add(client)
        _db.session.add(parking)
        _db.session.commit()

        yield _app
        _db.session.close()
        _db.drop_all()


@pytest.fixture
def client(app):
    client = app.test_client()
    yield client


@pytest.fixture
def db(app):
    with app.app_context():
        yield _db
