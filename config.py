import os
from dotenv import load_dotenv
load_dotenv()


class Config(object):
    DEBUG = False
    CSRF_ENABLED = True
    SECRET_KEY = 'MY_RANDOM_SECRET_KEY'
    SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URL')
    # SQLALCHEMY_ENGINE_OPTIONS = {'check_same_thread': False}
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class ProductionConfig(Config):
    DEBUG = False
    SQLALCHEMY_ECHO = True


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True
