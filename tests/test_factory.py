import pytest
from .conftest import client
from .factories import ClientFactory, ParkingFactory
from app import create_app, db as _db
from app.models import Client, Parking, ClientParking


def test_create_client(app, db):
    p_client = ClientFactory()
    db.session.commit()
    assert p_client.id is not None
    assert len(db.session.query(Client).all()) == 2


def test_create_parking(app, db):
    parking = ParkingFactory()
    db.session.commit()
    assert parking.id is not None
    assert len(db.session.query(Parking).all()) == 2
