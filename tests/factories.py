import factory
import factory.fuzzy as fuzzy
import random
from datetime import datetime

from app import db
from app.models import Client, Parking, ClientParking


class ClientFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Client
        sqlalchemy_session = db.session

    name = factory.Faker('first_name', locale='ru_RU')
    surname = factory.Faker('last_name', locale='ru_RU')
    credit_card = factory.Faker('credit_card_number')
    car_number = factory.Faker('bothify', text='?###??##', locale='ru_RU')


class ParkingFactory(factory.alchemy.SQLAlchemyModelFactory):

    class Meta:
        model = Parking
        sqlalchemy_session = db.session

    address = factory.Faker('address', locale='ru_RU')
    opened = factory.LazyAttribute(lambda x: random.choice([True, False]))
    count_places = factory.LazyAttribute(lambda x: random.randrange(0, 100))
    count_available_places = factory.LazyAttribute(lambda x: random.randrange(0, 100))


# class ClientParkingFactory(factory.alchemy.SQLAlchemyModelFactory):
#
#     class Meta:
#         model = ClientParking
#         sqlalchemy_session = db.session
#
#     client = factory.SubFactory(ClientFactory)
#     parking = factory.SubFactory(ParkingFactory)
#     time_in = datetime.utcnow()
