import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from dotenv import load_dotenv
load_dotenv()


db = SQLAlchemy()
# from .database import db


def create_app():
    app = Flask(__name__)
    app.config.from_object(os.getenv('APP_SETTINGS'))

    db.init_app(app)
    # with app.test_request_context():
    #     db.create_all()
    import app.parking.controllers as parking
    app.register_blueprint(parking.module)
    with app.app_context():
        db.create_all()

    # import app.parking.controllers as parking
    # app.register_blueprint(parking.module)
    return app