from . import db
from sqlalchemy.ext.associationproxy import association_proxy


class Client(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    surname = db.Column(db.String(50), nullable=False)
    credit_card = db.Column(db.String(50))
    car_number = db.Column(db.String(10))
    client_parking = db.relationship(
        "ClientParking",
        back_populates="client",
        cascade="all, delete-orphan",
    )
    parking = association_proxy("client_parking", "parking")

    def __str__(self):
        return f'{self.name}, {self.surname}, {self.credit_card}, {self.car_number}'

    def to_json(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Parking(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    address = db.Column(db.String(100), nullable=False)
    opened = db.Column(db.Boolean)
    count_places = db.Column(db.Integer, nullable=False)
    count_available_places = db.Column(db.Integer, nullable=False)
    client_parking = db.relationship(
        "ClientParking",
        back_populates="parking",
        cascade="all",
    )
    client = association_proxy("client_parking", "client")

    def to_json(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class ClientParking(db.Model):
    __tablename__ = "client_parking"

    id = db.Column(db.Integer, primary_key=True)
    client_id = db.Column(db.Integer, db.ForeignKey(Client.id), nullable=False)
    parking_id = db.Column(db.Integer, db.ForeignKey(Parking.id), nullable=False)
    time_in = db.Column(db.DateTime)
    time_out = db.Column(db.DateTime, nullable=True)
    client = db.relationship("Client", back_populates="client_parking", cascade="all")
    parking = db.relationship("Parking", back_populates="client_parking", cascade="all")

    def __init__(self,
                 client,
                 parking,
                 time_in,
                 time_out):
        self.client = client
        self.parking = parking
        self.time_in = time_in
        self.time_out = time_out

    def to_json(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
