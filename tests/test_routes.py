import json
import pytest


@pytest.mark.parametrize("route", ["/parking/clients",
                                   "/parking/clients/1",
                                   "/parking/parkings",
                                   "/parking/client_parkings"])
def test_route_status(client, route):
    rv = client.get(route)
    assert rv.status_code == 200


def test_create_client(client) -> None:
    client_data = {'name': 'Вася', 'surname': 'Пупкин', 'credit_card': None, 'car_number': 'y001yy77'}
    resp = client.post('/parking/clients', json=client_data, headers={'Content-Type': 'application/json'})

    assert resp.status_code == 200


def test_client(client) -> None:
    resp = client.get("/parking/clients/1")
    assert resp.status_code == 200
    assert resp.json == {'car_number': 'x777xx34',
                         'credit_card': '123456789',
                         'id': 1,
                         'name': 'Семен',
                         'parkings': [],
                         'surname': 'Семенов'}


def test_create_parking(client) -> None:
    parking_data = {'address': 'Молодежная 50',
                    'opened': True,
                    'count_available_places': 20,
                    'count_places': 20}
    resp = client.post('/parking/parkings', json=parking_data, headers={'Content-Type': 'application/json'})

    assert resp.status_code == 200
    assert resp.json.get('address') == 'Молодежная 50'


def test_parking(client) -> None:
    resp = client.get('/parking/parkings')
    assert resp.status_code == 200
    assert len(resp.json) == 1
    assert resp.json[0].get('address') == 'Советская 1а'


@pytest.mark.parking
def test_client_parking(client) -> None:
    client_parking_data = {'client_id': 1, 'parking_id': 1}
    resp = client.post('/parking/client_parkings',
                       json=client_parking_data, headers={'Content-Type': 'application/json'})
    resp_result = client.get('/parking/clients/1')
    assert resp_result.json.get('parkings')[0].get('id') == 1
