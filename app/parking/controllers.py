from flask import (
    Blueprint,
    render_template,
    request,
    abort,
    redirect,
    url_for,
    current_app,
)
from app.models import Client, Parking, ClientParking
from app import db
from datetime import datetime

module = Blueprint('parking', __name__, url_prefix='/parking')


@module.route('/', methods=['GET'])
def index():
    return 'Хелло вролд!'


@module.route('/clients', methods=['GET', 'POST'])
def clients_work():
    if request.method == 'GET':
        all_clients = Client.query.all()
        final_clients = [one_client.to_json() for one_client in all_clients]
        return final_clients
    elif request.method == 'POST':
        all_data = request.json
        new_client = Client(
            name=all_data.get('name'),
            surname=all_data.get('surname'),
            credit_card=all_data.get('credit_card'),
            car_number=all_data.get('car_number'),
        )
        db.session.add(new_client)
        db.session.commit()
        return new_client.to_json()
    return {'error': 'only GET and POST available'}


@module.route('/clients/<int:pk>', methods=['GET'])
def one_client(pk):
    one_client = Client.query.get(pk)
    one_client_parkings = [parking for parking in one_client.parking]
    print(one_client_parkings)
    this_parkings = [parking.to_json() for parking in one_client_parkings]
    this_client = one_client.to_json()
    this_client['parkings'] = this_parkings
    return this_client


@module.route('/parkings', methods=['GET', 'POST'])
def parkings_work():
    if request.method == 'GET':
        all_parkings = Parking.query.all()
        final_parkings = [one_parking.to_json() for one_parking in all_parkings]
        return final_parkings
    elif request.method == 'POST':
        all_data = request.json
        new_parking = Parking(
            address=all_data.get('address'),
            opened=True,
            count_places=all_data.get('count_places'),
            count_available_places=all_data.get('count_available_places')
        )
        db.session.add(new_parking)
        db.session.commit()
        return new_parking.to_json()
    return {'error': 'only GET and POST available'}


@module.route('/client_parkings', methods=['GET', 'POST', 'DELETE'])
def client_parkings_work():
    if request.method == 'GET':
        all_client_parkings = ClientParking.query.all()
        final_client_parkings = [one_client_parking.to_json() for one_client_parking in all_client_parkings]
        return final_client_parkings
    elif request.method == 'POST':
        all_data = request.json
        current_client = Client.query.get(all_data.get('client_id'))
        current_parking = Parking.query.get(all_data.get('parking_id'))
        if current_parking.count_available_places > 0 and current_parking.opened:
            new_client_parking = ClientParking(
                client=current_client,
                parking=current_parking,
                time_in=datetime.utcnow(),
                time_out=None
            )
            db.session.add(new_client_parking)
            current_parking.count_available_places = current_parking.count_available_places - 1
            db.session.add(current_parking)
            db.session.commit()
            return new_client_parking.to_json()
        return {'error': 'parking is full or parking closed'}
    elif request.method == 'DELETE':
        all_data = request.json
        current_client = Client.query.get(all_data.get('client_id'))
        current_parking = Parking.query.get(all_data.get('parking_id'))
        if current_client.credit_card:
            finished_parking = ClientParking.query.filter(
                ClientParking.client_id == current_client.id,
                ClientParking.parking_id == current_parking.id,
                ClientParking.time_out is None
            ).first()
            finished_parking.time_out = datetime.utcnow()
            db.session.commit()
            return finished_parking.to_json()
        return {'error': 'Client has not enough money'}
    return {'error': 'only GET, POST and DELETE available'}
