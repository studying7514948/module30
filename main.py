import os
from app import create_app

app = create_app()
# app.config.from_object(os.getenv('APP_SETTINGS'))
# manager = Manager(app)

if __name__ == '__main__':
    app.run()
